//
//  AddForumViewController.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 4/30/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class AddForumViewController: UIViewController {
    
    // MARK: Instance Variables
    
    var databaseReference: DatabaseReference?
    
    
    // MARK: Outlets
    
    @IBOutlet weak var forumNameTextField: UITextField!
    @IBOutlet weak var forumDescriptionTextField: UITextField!
    

    // MARK: View Controller Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        databaseReference = Database.database().reference()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        // Create a new forum in Firebase
        
        // Validation Checks
        if (forumNameTextField.text == nil) {
            
            showAlert(withAlertMessage: "Name Field Empty")
            return
        }
        
        if (forumDescriptionTextField.text == nil) {
            showAlert(withAlertMessage: "Add Atleast a line about the forum.")
            return
        }
        
        let nameString = forumNameTextField.text!
        var name = ""
        let arrayOfStrings = nameString.split(separator: " ")
        for str in arrayOfStrings {
            name += str
        }
        
        name = name.lowercased()
        
        let description = forumDescriptionTextField.text!
        
        storeForumOnFirebase(withName: name, description: description)
        
    }
    
    func storeForumOnFirebase(withName name: String, description: String) {
        
        let data = ["forumName": name, "forumDescription": description]
        
        // TODO: Check if the forum already exists
        
        databaseReference?.child("forums").child(name).setValue(data)
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    // MARK: Custom Functions
    
    // Show Alerts for errors in validation
    func showAlert(withAlertMessage alertMessage: String) {
        let alert = UIAlertController(title: "Error", message: alertMessage, preferredStyle: .alert)
        let action = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    

}
