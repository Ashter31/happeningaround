//
//  CategorySelectionViewController.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 5/5/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit

protocol CategorySelectionDelegate {
    func tappedWhichCategory(withCategory category: String)
}

class CategorySelectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: Properties
    
    var categories = ["all","business","general","health","science","sports","technology"]
    var delegate: CategorySelectionDelegate?

    // MARK: Outlets
    
    @IBOutlet weak var categoriesTableView: UITableView!
    
    // MARK: View Controller Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    // MARK: Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell",for: indexPath)
        
        cell.textLabel?.text = categories[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.tappedWhichCategory(withCategory: self.categories[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }

}
