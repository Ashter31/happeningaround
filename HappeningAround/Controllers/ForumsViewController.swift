//
//  ForumsViewController.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 4/30/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class ForumsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    
    

    // MARK: Instance Variables
    
    var databaseReference: DatabaseReference?
    fileprivate var _refHandle: DatabaseHandle!
    var forums: [DataSnapshot]! = []
    var searchController: UISearchController!
    var searchResults: [DataSnapshot]! = []
    
    
    // MARK: Outlets
    
    @IBOutlet weak var forumsTableView: UITableView!
    
    
    // MARK: View Controller Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureDatabase()
        
        searchController = UISearchController(searchResultsController: nil)
        forumsTableView.tableHeaderView = searchController.searchBar
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Custom Functions
    
    func configureDatabase() {
        databaseReference = Database.database().reference()
        
        // listen for new messages in the firebase database
        _refHandle = databaseReference?.child("forums").observe(.childAdded) { (snapshot: DataSnapshot) in
            self.forums.append(snapshot)
            self.forumsTableView.insertRows(at: [IndexPath(row: self.forums.count-1, section: 0)], with: .automatic)
            self.scrollToBottomMessage()
        }
    }
    
    func filterContent(for searchText: String) {
        searchResults = forums.filter({(DataSnapshot) -> Bool in
            let forum = DataSnapshot.value as! [String: String]
            let name = forum["forumName"] ?? "[forumName]"
            let isMatch = name.localizedCaseInsensitiveContains(searchText)
            print(isMatch)
            return isMatch
        })
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            filterContent(for: searchText)
            print(searchText)
            forumsTableView.reloadData()
        }
    }
    
    // MARK: Scroll Messages
    
    func scrollToBottomMessage() {
        if forums.count == 0 { return }
        let bottomMessageIndex = IndexPath(row: forumsTableView.numberOfRows(inSection: 0) - 1, section: 0)
        forumsTableView.scrollToRow(at: bottomMessageIndex, at: .bottom, animated: true)
    }
    
    
    // MARK: Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive {
            return searchResults.count
        } else {
            return forums.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "forumsCell", for: indexPath)
        
        let forumSnapshot = searchController.isActive ? searchResults[indexPath.row] : forums[indexPath.row]
        let forum = forumSnapshot.value as! [String: String]
        let name = forum["forumName"] ?? "[forumName]"
        
        cell.textLabel?.text = name
        
        return cell
    }

    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showForumMessages" {
            if let indexPath = forumsTableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! MessagesViewController
                
                let forumSnapshot = searchController.isActive ? searchResults[indexPath.row] : forums[indexPath.row]
                let forum = forumSnapshot.value as! [String: String]
                let name = forum["forumName"] ?? "[forumName]"
                let description = forum["forumDescription"] ?? "[forumDescription]"
                
                destinationController.forumName = name
                destinationController.forumDescription = description
            }
            
        }
    }

}
