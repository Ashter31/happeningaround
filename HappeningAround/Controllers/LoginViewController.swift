//
//  LoginViewController.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 4/27/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController, UITextFieldDelegate {
    // MARK: Instance Variables
    
    
    // MARK: Outlets

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    // MARK: View Controller Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Auth.auth().currentUser != nil {
            self.performSegue(withIdentifier: "loginToMainSegue", sender: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Button Actions
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        
        // Perform validation on the fields
        if (emailTextField.text == nil) {
            showAlert(withAlertMessage: "Email Field Empty")
            return
        }

        if (passwordTextField.text == nil) {
            showAlert(withAlertMessage: "Password Field Empty")
            return
        }
        
        // if valid, extract values
        let email = emailTextField.text!
        let password = passwordTextField.text!
    
        // Authenticate using the firebase for login
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in

            // Any errors while logging in
            if error != nil {
                print(error ?? "")
                self.showAlert(withAlertMessage: "Either Email exists or Password is wrong.")
                return
            }

            //successfully logged in
            self.performSegue(withIdentifier: "loginToMainSegue", sender: self)
        })
    }
    
    
    
    // MARK: Custom Functions
    
    // Show Alerts for errors in validation
    func showAlert(withAlertMessage alertMessage: String) {
        let alert = UIAlertController(title: "Error", message: alertMessage, preferredStyle: .alert)
        let action = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Delegates
    
    // When Return key is pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // when tapped elsewhere than fields
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}

