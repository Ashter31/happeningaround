//
//  MarketViewController.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 4/29/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit

class MarketViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: Constants
    
    let FROM_CODE = "1. From_Currency Code"
    let TO_CODE = "3. To_Currency Code"
    let EXCHANGE = "5. Exchange Rate"
    
    // MARK: Properties
    var currencyData = [["USD","EUR","GBP","AUD","CAD","INR","CNY","KRW"],["USD","EUR","GBP","AUD","CAD","INR","CNY","KRW"]]
    var stocksData: StockResults?
    var currencyResults: CurrencyResult?
    
    // MARK: Outlets
    
    @IBOutlet weak var stocksTableView: UITableView!
    @IBOutlet weak var fromPickerView: UIPickerView!
    @IBOutlet weak var currencyResultLabel: UILabel!
    
    // MARK: View Controller Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let apiString = "https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=USD&to_currency=USD&apikey=KWUD4FJ9XG4KU3LB"
        
        downloadJsonDataFromCurrencyApi(apiString: apiString) {
            let from = self.currencyResults?.data![self.FROM_CODE] ?? "NULL"
            let to = self.currencyResults?.data![self.TO_CODE] ?? "NULL"
            let exchange = self.currencyResults?.data![self.EXCHANGE] ?? "NULL"
            
            self.currencyResultLabel.text = "1 "+from+" = "+exchange+" "+to
        }
        downloadJsonDataFromStocksApi()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Custom Functions
    
    //loads all the data from the web API of CurrencyLayer
    
    func downloadJsonDataFromCurrencyApi(apiString: String,completed: @escaping () -> ()) {
        let url = URL(string: apiString)
        
        URLSession.shared.dataTask(with: url!) { (data,response,error) in
            if error == nil {
                guard let jsonData = data else { return }
                
                
                do {
                    self.currencyResults = try JSONDecoder().decode(CurrencyResult.self, from: jsonData)
                    DispatchQueue.main.async {
                        completed()
                    }
                } catch {
                    print("JSON Downloading Error")
                }
            }
            }.resume()
    }
    
    func downloadJsonDataFromStocksApi() {
        let apiString = "https://www.alphavantage.co/query?function=BATCH_STOCK_QUOTES&symbols=MSFT,ADBE,FB,AAPL,GOOGL,AMZN,SPOT&apikey=KWUD4FJ9XG4KU3LB"
        let url = URL(string: apiString)
        
        URLSession.shared.dataTask(with: url!) { (data,response,error) in
            if error == nil {
                guard let jsonData = data else { return }
                
                
                do {
                    self.stocksData = try JSONDecoder().decode(StockResults.self, from: jsonData)
                    OperationQueue.main.addOperation {
                        self.stocksTableView.reloadData()
                    }
                } catch {
                    print("JSON Downloading Error")
                }
            }
            }.resume()
    }
    
    // MARK: Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "stockCell", for: indexPath) as! StocksTableViewCell
        
        cell.companySymbolLabel?.text = stocksData?.stockquotes![indexPath.row].symbol
        cell.priceLabel.text = stocksData?.stockquotes![indexPath.row].price
        
        return cell
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return currencyData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currencyData[0].count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currencyData[component][row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            let fromP = currencyData[0][pickerView.selectedRow(inComponent: 0)]
            let toP = currencyData[1][pickerView.selectedRow(inComponent: 1)]
        
            let apiString = "https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency="+fromP+"&to_currency="+toP+"&apikey=KWUD4FJ9XG4KU3LB"
        
        downloadJsonDataFromCurrencyApi(apiString: apiString) {
            let from = self.currencyResults?.data![self.FROM_CODE] ?? "NULL"
            let to = self.currencyResults?.data![self.TO_CODE] ?? "NULL"
            let exchange = self.currencyResults?.data![self.EXCHANGE] ?? "NULL"
            
            self.currencyResultLabel.text = "1 "+from+" = "+exchange+" "+to
        }

    }

}
