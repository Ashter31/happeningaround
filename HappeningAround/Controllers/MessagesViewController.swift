//
//  MessagesViewController.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 4/30/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

class MessagesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UISearchResultsUpdating {
    
    
    
    // MARK: Properties
    
    var databaseReference: DatabaseReference?
    var storageReference: StorageReference?
    var messages: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle!
    var searchController: UISearchController!
    var searchResults: [DataSnapshot]! = []
    
    var forumName = ""
    var forumDescription = ""
    
    
    var userName = ""
    var currentUserMessageImageURL = ""
    
    
    // MARK: Outlets

    @IBOutlet weak var forumNameLabel: UILabel!
    @IBOutlet weak var forumDescriptionLabel: UILabel!
    @IBOutlet weak var messagesTableView: UITableView!
    @IBOutlet weak var messageTextField: UITextField!
    
    
    // MARK: View Controller Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        configureDatabase()
        messageTextField.delegate = self
        
        configureStorage()
        forumNameLabel.text = forumName
        forumDescriptionLabel.text = forumDescription
        
        searchController = UISearchController(searchResultsController: nil)
        messagesTableView.tableHeaderView = searchController.searchBar
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    @IBAction func didSendMessage(_ sender: UIButton) {
 
        if !messageTextField.text!.isEmpty {
            var data = ["messageContent": messageTextField.text! as String]
            data["messageUserName"] = userName
            data["profileImageUrl"] = currentUserMessageImageURL
            sendMessage(data: data)
        }
        
        let _ = textFieldShouldReturn(messageTextField)
        messageTextField.text = ""
    }
    
    // MARK: Custom Functions
    
    func filterContent(for searchText: String) {
        searchResults = messages.filter({(DataSnapshot) -> Bool in
            let message = DataSnapshot.value as! [String: String]
            let content = message["messageContent"] ?? "[messageContent]"
            let isMatch = content.localizedCaseInsensitiveContains(searchText)
            print(isMatch)
            return isMatch
        })
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            filterContent(for: searchText)
            print(searchText)
            messagesTableView.reloadData()
        }
    }
    
    func configureStorage() {
        storageReference = Storage.storage().reference()
        
        databaseReference?.child("users").child(Auth.auth().currentUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            let userName = value?["name"] as? String ?? "[user]"
            let imageUrl = value?["profileImageUrl"] as? String ?? ""
            
            self.userName = userName
            self.currentUserMessageImageURL = imageUrl
            
        })
    }
    
    func configureDatabase() {
        
        databaseReference = Database.database().reference()
        
        // listen for new messages in the firebase database
        _refHandle = databaseReference?.child("messages").child(forumName).observe(.childAdded) { (snapshot: DataSnapshot) in
            self.messages.append(snapshot)
            self.messagesTableView.insertRows(at: [IndexPath(row: self.messages.count-1, section: 0)], with: .automatic)
            self.scrollToBottomMessage()
        }
    }
    
    func sendMessage(data: [String:String]) {
        // add name to message and then data to firebase database
        
        databaseReference?.child("messages").child(forumName).childByAutoId().setValue(data)
    }
    
    func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let dismissAction = UIAlertAction(title: "Dismiss", style: .destructive, handler: nil)
            alert.addAction(dismissAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: Scroll Messages
    
    func scrollToBottomMessage() {
        if messages.count == 0 { return }
        let bottomMessageIndex = IndexPath(row: messagesTableView.numberOfRows(inSection: 0) - 1, section: 0)
        messagesTableView.scrollToRow(at: bottomMessageIndex, at: .bottom, animated: true)
    }
    
    
    // MARK: Delegates
    
    // Table View Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive {
            return searchResults.count
        } else {
            return messages.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "messageCell", for: indexPath) as! MessageTableViewCell
        
        let messageSnapshot = searchController.isActive ? searchResults[indexPath.row] : messages[indexPath.row]
        let message = messageSnapshot.value as? [String: String]
        
        let messageContent = message?["messageContent"] ?? "[message]"
        let messageUserName = message?["messageUserName"] ?? "[user]"
        
        print(Auth.auth().currentUser!.uid)
        let imageUrl = message?["profileImageUrl"] ?? "https://127.0.0.1"
        
        cell.messageNameLabel.text = messageUserName
        cell.messageContentLabel.text = messageContent
        
        Storage.storage().reference(forURL: imageUrl).getData(maxSize: INT64_MAX, completion: { (data, error) in
            guard error == nil else {
                print("Error downloading: \(error!)")
                return
            }
            let messageImage = UIImage.init(data: data!, scale: 50)
            
            // check if the cell is still on screen, if so, update cell image
            if cell == tableView.cellForRow(at: indexPath) {
                DispatchQueue.main.async {
                    cell.messageImageView?.image = messageImage
                    cell.setNeedsLayout()
                }
            }
        })
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let messageSnapshot = searchController.isActive ? searchResults[indexPath.row] : messages[indexPath.row]
            let message = messageSnapshot.value as? [String: String]
            
            let imageUrl = message?["profileImageUrl"] ?? "https://127.0.0.1"
            
            if (imageUrl == currentUserMessageImageURL) {
                let key = messages[indexPath.row].key
                databaseReference?.child("messages").child(forumName).child(key).removeValue()
                
                messages.remove(at: indexPath.row)
            }
            
            tableView.reloadData()
        }
    }
    
    // When Return key is pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // when tapped elsewhere than fields
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    

}
