//
//  NewPostViewController.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 5/4/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class NewPostViewController: UIViewController {
    
    // MARK: Properties
    
    var databaseReference: DatabaseReference?
    var userName: String?
    var currentUserPostImageURL: String?

    
    // MARK: Outlets
    
    @IBOutlet weak var postTextView: UITextView!
    
    // MARK: View Controller Cycle Functions
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getContent()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        databaseReference = Database.database().reference()
        postTextView.layer.borderWidth = 1.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    // MARK: Actions
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        
        if (postTextView.text == nil) {
            showAlert(title: "Error", message: "Blank Field")
            return
        }
        
        let postContent = postTextView.text!
        
        var post = [String: String]()
        
        post["postContent"] = postContent
        post["postUserName"] = userName
        post["profileImageUrl"] = currentUserPostImageURL
        
        sendMessage(data: post)
        
    }
    
    // MARK: Custom Functions
    
    func getContent() {
        databaseReference?.child("users").child(Auth.auth().currentUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            let userName = value?["name"] as? String ?? "[user]"
            let imageUrl = value?["profileImageUrl"] as? String ?? ""
            
            self.userName = userName
            self.currentUserPostImageURL = imageUrl
            
        })
    }
    
    func sendMessage(data: [String:String]) {
        // add name to message and then data to firebase database
        
        databaseReference?.child("posts").childByAutoId().setValue(data)
        dismiss(animated: true, completion: nil)
    }
    
    func showAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let dismissAction = UIAlertAction(title: "Dismiss", style: .destructive, handler: nil)
            alert.addAction(dismissAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
}
