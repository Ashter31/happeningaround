//
//  NewsDetailViewController.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 5/1/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit
import WebKit

class NewsDetailViewController: UIViewController {
    
    // MARK: Properties
    
    var urlToArticle: String?
    var webView: WKWebView!
    
    
    // MARK: View Controller Cycle Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false
        // Do any additional setup after loading the view.
        
        if let url = URL(string: urlToArticle!) {
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
    
    override func loadView() {
        webView = WKWebView()
        view = webView
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
