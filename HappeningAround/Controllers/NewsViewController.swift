//
//  NewsViewController.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 5/1/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class NewsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, NewsCollectionViewCellDelegate, CategorySelectionDelegate {

    // MARK: Properties
    
    var results = NewsResults()
    var databaseReference: DatabaseReference?
    var isLiked = [Bool]()
    
    // MARK: Outlets
    
    @IBOutlet weak var newsCollectionView: UICollectionView!
    
    
    // MARK: View Controller Cycle Functions
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if navigationController?.navigationBar.isHidden == false {
            navigationController?.navigationBar.isHidden = true
        }
        
        let defaultUrl = "https://newsapi.org/v2/top-headlines?country=us&apiKey=b133bc687b604392897ad8394100d3f5"
        downloadJsonDataFromNewsApi(url: defaultUrl)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        databaseReference = Database.database().reference()
        
        newsCollectionView.allowsMultipleSelection = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: Custom Functions
    
    //loads all the data from the web API of News
    func downloadJsonDataFromNewsApi(url: String) {
        let url = URL(string: url)
        
        URLSession.shared.dataTask(with: url!) { (data,response,error) in
            if error == nil {
                guard let jsonData = data else { return }
                
                do {
                    self.results = try JSONDecoder().decode(NewsResults.self, from: jsonData)
                    OperationQueue.main.addOperation {
                        self.newsCollectionView.reloadData()
                    }
                } catch {
                    print("JSON Downloading Error")
                }
            }
        }.resume()
    }
    
    // MARK: Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = results.articles?.count {
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newsCell", for: indexPath) as! NewsCollectionViewCell
        
        cell.newsTitleLabel.text = results.articles![indexPath.row].title
        
        cell.newsDescriptionLabel.text = results.articles![indexPath.row].description
        
        if let imagePath = results.articles![indexPath.row].urlToImage {
           cell.newsImageView.downloadImageFromUrl(path: imagePath)
        } else {
            cell.newsImageView.image = #imageLiteral(resourceName: "notAvailable")
        }
        
        
        cell.url = results.articles![indexPath.row].url
        
        cell.delegate = self
        
        
        // check if the article is already liked
        databaseReference?.child("favoriteNews").child(Auth.auth().currentUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in

            if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                for snap in snapshots {
                    let value = snap.value as? NSDictionary
                    let ctitle = value?["title"] as? String ?? ""
                    if (ctitle == self.results.articles![indexPath.row].title) {
                        cell.newsLikeButton.setTitle("👍", for: UIControlState())
                    } else {
                        cell.newsLikeButton.setTitle("Like", for: UIControlState())
                    }
                }
            }
        })
        
        
        return cell
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "newsCellToDetailSegue" {
            if let indexPath = newsCollectionView.indexPathsForSelectedItems {
                let destinationController = segue.destination as! NewsDetailViewController
                
                let url = results.articles![indexPath[0].row].url
                
                destinationController.urlToArticle = url
            }
            
        }
        
        if segue.identifier == "chooseCategorySegue" {
            let destinationModal = segue.destination as! CategorySelectionViewController
            
            destinationModal.delegate = self
            
        }
    }
    
    
    // MARK: Custom Delegates
    
    func didTappedLikeButton(withSender sender: UIButton, title: String) {
        
        if (sender.currentTitle != "👍") {
        
            var news: News?
            
            for value in results.articles! {
                if (title == value.title) {
                    news = value
                }
            }
            
            var article = [String:String]()
            article["author"] = news?.author
            article["url"] = news?.url
            article["urlToImage"] = news?.urlToImage
            article["title"] = news?.title
            article["description"] = news?.description
            article["publishedAt"] = news?.publishedAt
            
            article["id"] = news?.source?.id
            article["name"] = news?.source?.name
            
            
            databaseReference?.child("favoriteNews").child(Auth.auth().currentUser!.uid).childByAutoId().setValue(article)
            
            sender.setTitle("👍", for: UIControlState())
        } else {
            databaseReference?.child("favoriteNews").child(Auth.auth().currentUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                var key: String?
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                    for snap in snapshots {
                        let value = snap.value as? NSDictionary
                        let ctitle = value?["title"] as? String ?? ""
                        if (title ==  ctitle) {
                           key = snap.key
                            print(snap.key)
                        } else {
                            key = ""
                        }
                    }
                }
                
                // Delete the value and set the title to "Like"
                self.databaseReference?.child("favoriteNews").child(Auth.auth().currentUser!.uid).child(key!).removeValue()
                sender.setTitle("Like", for: UIControlState())
            })
        }
    }
    
    func didTappedShareButton(withTitle title: String, url: String) {
        displayShareSheet(shareContent: title+"\n"+url)
    }
    
    func displayShareSheet(shareContent:String) {
        let activityViewController = UIActivityViewController(activityItems: [shareContent as NSString], applicationActivities: nil)
        present(activityViewController, animated: true, completion: {})
    }
    
    func tappedWhichCategory(withCategory category: String) {
        print(category)
        if category == "all" {
            let url = "https://newsapi.org/v2/top-headlines?country=us&apiKey=b133bc687b604392897ad8394100d3f5"
            downloadJsonDataFromNewsApi(url: url)
        } else {
            let url = "https://newsapi.org/v2/top-headlines?country=us&category="+category.lowercased()+"&apiKey=b133bc687b604392897ad8394100d3f5"
            downloadJsonDataFromNewsApi(url: url)
        }
    }

}
