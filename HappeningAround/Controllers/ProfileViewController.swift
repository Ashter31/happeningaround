//
//  ProfileViewController.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 4/29/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import CoreLocation
import MapKit

class ProfileViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate {
    
    
    // MARK: Instance Variables
    lazy var imagePicker = UIImagePickerController()
    var databaseReference: DatabaseReference?
    var storageReference: StorageReference?
    var locationManager = CLLocationManager()
    var latitude: CLLocationDegrees?
    var longitude: CLLocationDegrees?

    // MARK: Outlets
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    
    // MARK: View Controller Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // create tap gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(profileImagePressed(gesture:)))
        
        // add it to the image view
        profileImage.addGestureRecognizer(tapGesture)
        // make sure imageView can be interacted with by user
        profileImage.isUserInteractionEnabled = true
        
        
        imagePicker.delegate = self
        nameTextField.delegate = self
        cityTextField.delegate = self
        countryTextField.delegate = self
        
        profileImage.image = #imageLiteral(resourceName: "profileIcon")
        storePhotoOnStorage(imageData: UIImageJPEGRepresentation(profileImage.image!, 0.8)!)
        
        databaseReference = Database.database().reference()
        storageReference = Storage.storage().reference()
        
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        } else {
            print("Location services are not enabled");
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - CoreLocation Delegate Methods
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        latitude = coord.latitude
        longitude = coord.longitude
    }
    
    func reverseGeocoding(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        let location = CLLocation(latitude: latitude, longitude: longitude)
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                print(error ?? "[]")
                return
            }
            else if (placemarks?.count)! > 0 {
                let pm = placemarks![0]
                
                self.cityTextField.text = pm.locality
                self.countryTextField.text = pm.country
            }
        })
    }
    
    // MARK: Actions
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        // Validations Checks for the fields
        if (nameTextField.text == nil) {
            showAlert(withAlertMessage: "Empty Name Field")
            return
        }
        
        if (cityTextField.text == nil) {
            showAlert(withAlertMessage: "Empty City Field")
            return
        }
        
        if (countryTextField.text == nil) {
            showAlert(withAlertMessage: "Empty Country Field")
            return
        }
        
        if (profileImage.image == #imageLiteral(resourceName: "profileIcon")) {
            showAlert(withAlertMessage: "Empty Profile Image")
            return
        }
        
        //extract values from fields
        let name = nameTextField.text!
        let city = cityTextField.text!
        let country = countryTextField.text!


        // Store the user data
        storeUserDataOnDatabase(withName: name, city: city, country: country)
        
        // Navigate to Main Screen
        self.performSegue(withIdentifier: "profileToMainSegue", sender: self)
        
    }
    
    @IBAction func findMeButtonPressed(_ sender: UIButton) {
 
        reverseGeocoding(latitude: latitude!, longitude: longitude!)
    }
    
    @objc func profileImagePressed(gesture: UIGestureRecognizer) {
        if (gesture.view as? UIImageView) != nil {
            print("Image Tapped")

            // Implement the image Picker
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            imagePicker.modalPresentationStyle = .popover
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    // MARK: Custom Functions
    
    // Show Alerts for errors in validation
    func showAlert(withAlertMessage alertMessage: String) {
        let alert = UIAlertController(title: "Error", message: alertMessage, preferredStyle: .alert)
        let action = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    func storePhotoOnStorage(imageData: Data) {

        let imagePath = "user_photos/" + Auth.auth().currentUser!.uid + "/\(Auth.auth().currentUser!.uid).jpg"

        // set content type to “image/jpeg” in firebase storage metadata
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"

        // create a child node at imagePath with imageData and metadata
        storageReference?.child(imagePath).putData(imageData, metadata: metadata) { (metadata, error) in

            if let error = error {
                print("Error uploading: \(error)")
                return
            }
            

            // use sendMessage to add imageURL to database
            self.storeImageDataOnDatabase(imageData:  ["profileImageUrl":(self.storageReference?.child((metadata?.path)!).description)!])
        }
    }
    
    func storeImageDataOnDatabase(imageData: [String: String]) {
        let imagePath = "users/" + Auth.auth().currentUser!.uid + "/"
        databaseReference?.child(imagePath).setValue(imageData)
    }

    func storeUserDataOnDatabase(withName name: String, city: String, country: String) {
        print("function Called")
        databaseReference?.child("users").child(Auth.auth().currentUser!.uid).child("name").setValue(name)
        databaseReference?.child("users").child(Auth.auth().currentUser!.uid).child("city").setValue(city)
        databaseReference?.child("users").child(Auth.auth().currentUser!.uid).child("country").setValue(country)
    }
    
    
    
    // MARK: Delegates
    
    //What to do when the picker returns with a photo
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let photoData = UIImageJPEGRepresentation(chosenImage, 0.8)
            profileImage.contentMode = .scaleAspectFill
            profileImage.clipsToBounds = true
            profileImage.image = chosenImage
            if let data = photoData {
                self.storePhotoOnStorage(imageData: data)
            }
            
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    //What to do if the image picker cancels.
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true,completion: nil)
    }
    
    // When Return key is pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // when tapped elsewhere than fields
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
