//
//  RegisterViewController.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 4/27/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    
    // MARK: Outlets
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    
    // MARK: View Controller Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //assign delegates
        emailTextField.delegate = self
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Button Actions
    
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        // Create a new user & navigate to main screen
        
        // Validations checks
        if (emailTextField.text == nil) {
            showAlert(withAlertMessage: "Email Field Empty.")
            return
        }
        
        if (passwordTextField.text == nil) {
            showAlert(withAlertMessage: "Password Field Empty.")
            return
        }
        
        if (confirmPasswordTextField.text == nil || confirmPasswordTextField.text != passwordTextField.text) {
            showAlert(withAlertMessage: "Passwords do not match.")
            return
        }
        
        
        // if valid, extract values
        let email = emailTextField.text!
        let password = passwordTextField.text!
        
        if (password.lengthOfBytes(using: .ascii) < 6) {
            showAlert(withAlertMessage: "Pasword should be of length 6 or greater.")
            return
        }
        
        
        // authenticate and create a new user
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            
            if error != nil {
                print(error ?? "")
                self.showAlert(withAlertMessage: "Invalid Email or Invalid Password.")
                return
            }
            
            guard (user?.uid) != nil else {
                return
            }
            
            // TODO: Perform Segue to the Main Screen
            self.performSegue(withIdentifier: "registerToProfileSegue", sender: self)
        })
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        // Navigate back to the Login Screen
        self.dismiss(animated: true, completion: nil)
    }

    
    // MARK: Custom Functions
    
    // Show Alerts for errors in validation
    func showAlert(withAlertMessage alertMessage: String) {
        let alert = UIAlertController(title: "Error", message: alertMessage, preferredStyle: .alert)
        let action = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }

    
    //MARK: - Delegates
    
    // When Return key is pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // when tapped elsewhere than fields
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}
