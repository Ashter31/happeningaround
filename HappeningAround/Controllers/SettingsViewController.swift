//
//  SettingsViewController.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 4/29/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit
import FirebaseAuth

class SettingsViewController: UIViewController {
    
    // MARK: Instance Variables
    
    var data = ["Your Account","About Us","Help & FAQs"]
    
    // MARK: View Controller Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    @IBAction func logoutButtonPressed(_ sender: UIButton) {
        // Sign-out!!!
        do {
            try Auth.auth().signOut()
        } catch let logoutError {
            print(logoutError)
        }
        
        checkIfSignedIn()
    }
    
    // MARK: Custom Functions
    
    func checkIfSignedIn() {
        if Auth.auth().currentUser == nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initial = storyboard.instantiateInitialViewController()
            UIApplication.shared.keyWindow?.rootViewController = initial
        }
    }
    
    @IBAction func close(segue: UIStoryboardSegue) {
        
    }
    

}
