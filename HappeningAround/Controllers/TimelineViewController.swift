//
//  TimelineViewController.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 5/3/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class TimelineViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    // MARK: Properties
    
    var databaseReference: DatabaseReference?
    var storageReference: StorageReference?
    var posts: [DataSnapshot]! = []
    fileprivate var _refHandle: DatabaseHandle!
    
    
    var userName = ""
    var currentUserPostImageURL = ""
    
    
    // MARK: Outlets
    
    @IBOutlet weak var postsTableView: UITableView!
    
    // MARK: View Controller Cycle Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        configureDatabase()
        configureStorage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    
    // MARK: Custom Functions
    
    func configureStorage() {
        storageReference = Storage.storage().reference()
        
        databaseReference?.child("users").child(Auth.auth().currentUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            let userName = value?["name"] as? String ?? "[user]"
            let imageUrl = value?["profileImageUrl"] as? String ?? ""
            
            self.userName = userName
            self.currentUserPostImageURL = imageUrl
            
        })
    }
    
    func configureDatabase() {
        
        databaseReference = Database.database().reference()
        
        // listen for new messages in the firebase database
        _refHandle = databaseReference?.child("posts").observe(.childAdded) { (snapshot: DataSnapshot) in
            self.posts.append(snapshot)
            self.postsTableView.insertRows(at: [IndexPath(row: self.posts.count-1, section: 0)], with: .automatic)
            self.scrollToBottomPost()
        }
    }
    
    // MARK: Scroll Messages
    
    func scrollToBottomPost() {
        if posts.count == 0 { return }
        let bottomPostIndex = IndexPath(row: postsTableView.numberOfRows(inSection: 0) - 1, section: 0)
        postsTableView.scrollToRow(at: bottomPostIndex, at: .bottom, animated: true)
    }
    
    
    // MARK: Delegates
    
    // Table View Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "timelineCell", for: indexPath) as! TimelineTableViewCell
        
        let postSnapshot = posts[indexPath.row]
        let post = postSnapshot.value as? [String: String]
        
        let postContent = post?["postContent"] ?? "[message]"
        let postUserName = post?["postUserName"] ?? "[user]"

        let imageUrl = post?["profileImageUrl"] ?? "https://127.0.0.1"
        
        cell.timelineUserNameLabel.text = postUserName
        cell.timelineStatusLabel.text = postContent
        cell.timelineStatusLabel.sizeToFit()
        
        Storage.storage().reference(forURL: imageUrl).getData(maxSize: INT64_MAX, completion: { (data, error) in
            guard error == nil else {
                print("Error downloading: \(error!)")
                return
            }
            let postImage = UIImage.init(data: data!, scale: 50)
            
            // check if the cell is still on screen, if so, update cell image
            if cell == tableView.cellForRow(at: indexPath) {
                DispatchQueue.main.async {
                    cell.timelineUserImageView?.image = postImage
                    cell.setNeedsLayout()
                }
            }
        })
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let postSnapshot = posts[indexPath.row]
            let post = postSnapshot.value as? [String: String]
            
            let imageUrl = post?["profileImageUrl"] ?? "https://127.0.0.1"
            
            if (imageUrl == currentUserPostImageURL) {
                let key = posts[indexPath.row].key
                databaseReference?.child("posts").child(key).removeValue()
                
                posts.remove(at: indexPath.row)
            }
            
            tableView.reloadData()
        }
    }

}
