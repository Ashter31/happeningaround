//
//  Currency.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 5/5/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import Foundation

struct Currency: Decodable {
    var fromCode: String?
    var fromName: String?
    var toCode: String?
    var toName: String?
    var exchange: String?
    
    enum CodingKeys: String, CodingKey {
        case fromCode = "1. From_Currency Code"
        case fromName = "2. From_Currency Name"
        case toCode = "3. To_Currency Code"
        case toName = "4. To_Currency Name"
        case exchange = "5. Exchange Rate"
    }
}

struct CurrencyResult: Decodable {
    var data: [String : String]?
    
    enum CodingKeys: String, CodingKey {
        case data = "Realtime Currency Exchange Rate"
    }
}
