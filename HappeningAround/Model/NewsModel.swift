//
//  NewsResults.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 5/1/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import Foundation

struct News: Decodable {
    
    let source: Source?
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    
}

struct Source: Decodable {
    let id: String?
    let name: String?
}

struct NewsResults: Decodable {
    var status: String?
    var totalResults: Int?
    var articles: [News]?
    
    init() {
        
    }
}
