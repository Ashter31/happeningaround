//
//  Stocks.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 5/5/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import Foundation

struct Stock: Decodable {
    var symbol: String?
    var price: String?

    enum CodingKeys: String, CodingKey {
        case symbol = "1. symbol"
        case price = "2. price"
    }
}

struct StockResults: Decodable {
    var stockquotes: [Stock]?
    
    enum CodingKeys: String, CodingKey {
        case stockquotes = "Stock Quotes"
    }
}
