//
//  MessageTableViewCell.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 4/30/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {
    
    // MARK: Outlets

    @IBOutlet weak var messageImageView: UIImageView!
    @IBOutlet weak var messageNameLabel: UILabel!
    @IBOutlet weak var messageContentLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        messageImageView.layer.cornerRadius = 30
        messageImageView.clipsToBounds = true
        messageImageView.contentMode = .scaleAspectFill
    }

}
