//
//  NewsCollectionViewCell.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 5/1/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit

protocol NewsCollectionViewCellDelegate {
    
    func didTappedLikeButton(withSender sender: UIButton, title: String)
    func didTappedShareButton(withTitle title: String, url: String)
}

class NewsCollectionViewCell: UICollectionViewCell {
    
    // MARK: Properties
    
    var delegate: NewsCollectionViewCellDelegate?
    var url: String?
    
    // MARK: Outlets
    
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsDescriptionLabel: UILabel!
    @IBOutlet weak var newsLikeButton: UIButton!

    // MARK: Button Actions
    
    @IBAction func likeButtonPressed(_ sender: UIButton) {
        delegate?.didTappedLikeButton(withSender: sender, title: newsTitleLabel.text!)
    }
   
    @IBAction func shareButtonPressed(_ sender: UIButton) {
        delegate?.didTappedShareButton(withTitle: newsTitleLabel.text!, url: url!)
    }
}
