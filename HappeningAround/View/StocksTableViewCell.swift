//
//  StocksTableViewCell.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 4/29/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit

class StocksTableViewCell: UITableViewCell {

    // MARK: Outlets
    
    @IBOutlet weak var companySymbolLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
