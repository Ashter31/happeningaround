//
//  TimelineTableViewCell.swift
//  HappeningAround
//
//  Created by Aarsh Patil on 5/3/18.
//  Copyright © 2018 Aarsh Patil. All rights reserved.
//

import UIKit

class TimelineTableViewCell: UITableViewCell {

    @IBOutlet weak var timelineUserImageView: UIImageView!
    @IBOutlet weak var timelineUserNameLabel: UILabel!
    @IBOutlet weak var timelineStatusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        timelineUserImageView.contentMode = .scaleAspectFill
        timelineUserImageView.clipsToBounds = true
        timelineUserImageView.layer.cornerRadius = 26
    }

}
