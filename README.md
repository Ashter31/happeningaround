# Happening Around

## Installation

1. Import the project to local directory or clone it using 'git clone'.
2. Install all the dependencies using the CocoaPods.
3. Create a new project in the Google Firebase account, download the google services info file, and keep it under the main directory of XCode project.
4. Enable Authentication using email in the app and set the security rules of the authentication as well as realtime database.
5. Build the project using Xcode and install on the Simulator or iPhone. 
